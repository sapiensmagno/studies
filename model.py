# -*- coding: utf-8 -*-

from flask import current_app
import hashlib
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):

    __tablename__ = 'ACL'

    id = db.Column(db.Integer, db.Sequence('user_id_seq'), primary_key=True)
    username = db.Column(db.String(255))
    passwd = db.Column(db.String(64))

    def __init__(self, username, password):
        self.username = username
        self.password = password

    @property
    def password(self):
        return self.passwd
    
    @password.setter
    def password(self, value):
        self.passwd = hashlib.sha256(value).hexdigest()
        
    @classmethod
    def try_auth(cls, username, password):
        wanted_user = cls(username, password)
        cls.query.filter_by(username=wanted_user.username, passwd=wanted_user.password).one()