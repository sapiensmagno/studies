# *-* coding: utf-8 *-*
# Testing login.py
import os
import unittest
import tempfile
import base64

# Both 'self.assert' and 'assert' work. What's the difference?
# Is tearDown unecessary?
class LoginTestCase(unittest.TestCase):

    def setup_db(self):
        with self.app.app_context():
            self.app.db.create_all()

    def setUp(self):
        os.environ["APP_LOGIN_CONFIG"] = "testing"
        import login
        self.app = login.app

        self.setup_db()
        self.client = self.app.test_client()


    def register_user(self, usr='foo', passwd='bar'):
        print 'registering usr: ' , usr, ' pass: ' , passwd
        return self.client.post("/register", data={"username": usr,
                                              "password": passwd})

    def login(self, username, password):
        print 'loging in usr: ' + username + ' pass: ' + password
        return self.client.open('/hell/', 'GET', headers={'Authorization': 'Basic ' + 
                                                base64.b64encode(username + ":" + password)})

    def test_register_user(self):
        response = self.register_user()
        print 'register response data: ', response.data
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, "Now you can go to hell with your new user")

    def test_login(self):
        self.register_user()

        response = self.login('wfoo', 'wbar')
        print 'test_login - wrong username and password - response data: ', response.data
        assert 'WTF r u trying to do?' in response.data
        assert response.status_code == 401

        response = self.login("foo", "bar")
        print 'test_login - correct username and password - response data: ', response.data
        assert 'hell' in response.data
        assert response.status_code == 200


if __name__ == '__main__':
    unittest.main() 
#
#    def tearDown(self):
#        pass
#
#    @skip
#    def test_failing(self):
#       self.assertFalse(True)
#
#       self.assertFalse(False)
#
#    def test_foo(self):
#        self.assertEqual(foo.incr(4), 5)