# -*- coding: utf-8 -*-

# import ipdb
from functools import wraps
import os
# from jinja2 import Template
from flask import Flask, request, abort, Response, render_template
# template1 = render_template('Hello {{ name }}! authenticate and go to hell', name = 'JBG')
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from model import User, db


def create_app(deploy_type='develop'):
    
    app = Flask(__name__)

    if deploy_type == 'develop':
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///logintest.db'
        app.debug = True

    elif deploy_type == "testing":
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.config["TESTING"] = True
        app.debug = True

    ## TODO: create an else for production ASAP, pls

    db.init_app(app)
    app.db = db
    return app


app = create_app(os.environ.get("APP_LOGIN_CONFIG", "develop"))


def check_auth():
    try:
        auth = request.authorization
        User.try_auth(auth.username, auth.password)
        return
    except (NoResultFound, MultipleResultsFound, AttributeError):
        # auth or current.user may be None. 
        pass
    else:
        return
    abort(401)


@app.errorhandler(401)
def authenticate(error):
    return Response('WTF r u trying to do?', 401, {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        check_auth()
        return f(*args, **kwargs)
    return decorated


@app.route('/')
def helloween():
    # When using jinja2 through flask, you must give render_template the name of a template file located in /templates
    # To render a template from a string, you must do "from jinja2 import Template" and use "Template.render('string')"
    return render_template('Hello {{ name }}! authenticate and go to hell', name = 'JBG')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        new_usr = User(request.form['username'], request.form['password'])
        db.session.add(new_usr)
        db.session.commit()
        return 'Now you can go to hell with your new user'
    else:
        return render_template('usr_reg.html') # show registration form

@app.route('/hell/')
@requires_auth
def hell():
    return 'hell'

if __name__ == "__main__":
    app.run(host="0.0.0.0")
